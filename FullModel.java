package Assignment2;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloRange;
import ilog.cplex.IloCplex;

/**
 *
 * @author Gustav
 */
public class FullModel {
    
    private final IloCplex model;
    private final IloIntVar u[][];
    private final IloNumVar c[][];
    private final IloNumVar p[][];
    private final IloNumVar l[];
    private final UnitCommitmentProblem ucp;
    private final IloRange startupCostConstraint[][];
    private final IloRange minUptimeConstraint[][];
    private final IloRange minDowntimeConstraint[][];
    private final IloRange powerBalanceConstraint[];
    private final IloRange minProductionConstraint[][];
    private final IloRange maxProductionConstraint[][];
    private final IloRange rampUpConstraint[][];
    private final IloRange rampDownConstraint[][];
    
/**
     * Creates the full model for the original problem
     * (without decomposition)
     * @param ucp
     * @throws IloException 
     */
    public FullModel(UnitCommitmentProblem ucp) throws IloException {
        this.ucp = ucp;
        
        // 1. Every model needs an IloCplex object
        this.model = new IloCplex();
        
        // 2. Creates the decision variables
        this.u = new IloIntVar[ucp.getnUnits()][ucp.getnTimePeriods()];
        
        for(int g = 1; g<= ucp.getnUnits(); g++){
            for(int t = 1; t<= ucp.getnTimePeriods(); t++){
            u[g-1][t-1] = model.boolVar();
            }
        }
   
        this.c = new IloNumVar[ucp.getnUnits()][ucp.getnTimePeriods()];
        
        for(int g = 1; g<= ucp.getnUnits(); g++){
            for(int t = 1; t<= ucp.getnTimePeriods(); t++){
            c[g-1][t-1] = model.numVar(0,Double.POSITIVE_INFINITY,"c");
            }
        }
        
        this.p = new IloNumVar[ucp.getnUnits()][ucp.getnTimePeriods()];
        
        for(int g = 1; g<= ucp.getnUnits(); g++){
            for(int t = 1; t<= ucp.getnTimePeriods(); t++){
            p[g-1][t-1] = model.numVar(0,Double.POSITIVE_INFINITY,"p");
            }
        }
        
         this.l = new IloNumVar[ucp.getnTimePeriods()];
        
        for(int t = 1; t<= ucp.getnTimePeriods(); t++){
            l[t-1] = model.numVar(0,Double.POSITIVE_INFINITY,"l");
        }
       
        // 3. Creates the objective function
        // Creates an empty linear numerical expression (linear equation)
        IloLinearNumExpr obj = model.linearNumExpr();
        
        // Adds terms to the equation
        for(int g = 1; g<= ucp.getnUnits(); g++){
            for(int t = 1; t<= ucp.getnTimePeriods(); t++){
                obj.addTerm(1,c[g-1][t-1]);
                obj.addTerm(ucp.getCommitmentCosts()[g-1],u[g-1][t-1]);  
                obj.addTerm(ucp.getProductionCosts()[g-1],p[g-1][t-1]); 
            }
        }
        for(int t = 1; t<= ucp.getnTimePeriods(); t++){
            obj.addTerm(ucp.getLoadSheddingCost()[t-1],l[t-1]);            
        }
                
        // Tells cplex to minimize the objective function
        model.addMinimize(obj);
               
        // 4. Creates the constraints
        // For each constraints creates an populates a linear equation
             
        // Startup cost constraint 
        this.startupCostConstraint = new IloRange[ucp.getnUnits()][ucp.getnTimePeriods()];
        for(int g = 1; g<= ucp.getnUnits(); g++){
            for(int t = 1; t<= ucp.getnTimePeriods(); t++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(1, c[g-1][t-1]);
                IloLinearNumExpr rhs = model.linearNumExpr();
                rhs.addTerm(ucp.getStartupCosts()[g-1], u[g-1][t-1]);
                if(t >= 2){rhs.addTerm(-ucp.getStartupCosts()[g-1], u[g-1][t-2]);}else{}
                startupCostConstraint[g-1][t-1] = (IloRange) model.addGe(lhs, rhs);
            }
        }
        // Minimum operation time Constraint        
        this.minUptimeConstraint = new IloRange[ucp.getnUnits()][ucp.getnTimePeriods()];    
        for(int g = 1; g<= ucp.getnUnits(); g++){
            for(int t = 1; t<= ucp.getnTimePeriods(); t++){
                 double T = Math.min(t+ucp.getMinUptime()[g-1]-1, ucp.getnTimePeriods());
                 IloLinearNumExpr lhs = model.linearNumExpr();
                 for(int i = t; i<= T; i++){
                 lhs.addTerm(1, u[g-1][i-1]);
                 lhs.addTerm(-1, u[g-1][t-1]);
                 if(t >= 2){lhs.addTerm(1, u[g-1][t-2]);}else{}
                 }
                 minUptimeConstraint[g-1][t-1] = (IloRange) model.addGe(lhs,0);
            }
        }
              
        // Minimum down time constraint
        this.minDowntimeConstraint = new IloRange[ucp.getnUnits()][ucp.getnTimePeriods()];
        for(int g = 1; g<= ucp.getnUnits(); g++){
            for(int t = 1; t<= ucp.getnTimePeriods(); t++){
                double T = Math.min(t+ucp.getMinDowntime()[g-1]-1, ucp.getnTimePeriods());
                 IloLinearNumExpr lhs = model.linearNumExpr();
                 for(int i = t; i<= T; i++){
                 lhs.setConstant(1);
                 lhs.addTerm(1, u[g-1][i-1]);
                 lhs.addTerm(-1, u[g-1][t-1]);
                 if(t >= 2){lhs.addTerm(1, u[g-1][t-2]);}else{}
                 } 
                 minDowntimeConstraint[g-1][t-1] = (IloRange) model.addGe(lhs,0);
            }
        }
        
        // Power balance constraint
        this.powerBalanceConstraint = new IloRange[ucp.getnTimePeriods()];
        for(int t = 1; t<= ucp.getnTimePeriods(); t++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                for(int g = 1; g<= ucp.getnUnits(); g++){
                    lhs.addTerm(1, p[g-1][t-1]);
                }
                lhs.addTerm(1, l[t-1]);
                powerBalanceConstraint[t-1] = (IloRange) model.addEq(lhs, ucp.getLoads()[t-1]);
            }
        
        
        // Minimium production constraint
        this.minProductionConstraint = new IloRange[ucp.getnUnits()][ucp.getnTimePeriods()];
        for(int g = 1; g<= ucp.getnUnits(); g++){
            for(int t = 1; t<= ucp.getnTimePeriods(); t++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(1, p[g-1][t-1]);
                IloLinearNumExpr rhs = model.linearNumExpr();
                rhs.addTerm(ucp.getPowerLB()[g-1], u[g-1][t-1]);
                minProductionConstraint[g-1][t-1] = (IloRange) model.addGe(lhs, rhs);
            }
        }
        
        // Maximum production constraint
        this.maxProductionConstraint = new IloRange[ucp.getnUnits()][ucp.getnTimePeriods()];
        for(int g = 1; g<= ucp.getnUnits(); g++){
            for(int t = 1; t<= ucp.getnTimePeriods(); t++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(1, p[g-1][t-1]);
                IloLinearNumExpr rhs = model.linearNumExpr();
                rhs.addTerm(ucp.getPowerUB()[g-1], u[g-1][t-1]);
                maxProductionConstraint[g-1][t-1] = (IloRange) model.addLe(lhs, rhs);
            }
        }
        
        // Ramp-up limit constraint
        this.rampUpConstraint = new IloRange[ucp.getnUnits()][ucp.getnTimePeriods()];
        for(int g = 1; g<= ucp.getnUnits(); g++){
            for(int t = 1; t<= ucp.getnTimePeriods(); t++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(1, p[g-1][t-1]);
                if(t >= 2){lhs.addTerm(-1, p[g-1][t-2]);}else{}
                rampUpConstraint[g-1][t-1] = (IloRange) model.addLe(lhs, ucp.getRamping()[g-1]);
            }
        }
        
        // Ramp-down limit constraint
        this.rampDownConstraint = new IloRange[ucp.getnUnits()][ucp.getnTimePeriods()];
        for(int g = 1; g<= ucp.getnUnits(); g++){
            for(int t = 1; t<= ucp.getnTimePeriods(); t++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                if(t >= 2){lhs.addTerm(1, p[g-1][t-2]);}else{}
                lhs.addTerm(-1, p[g-1][t-1]);
                rampDownConstraint[g-1][t-1] = (IloRange) model.addLe(lhs, ucp.getRamping()[g-1]);
            }
        }
    }
    // solves the full model and prints the objective value
    public void solve() throws IloException{
        model.setOut(null);
        model.solve();
        System.out.println("Optimal objective value "+model.getObjValue());
        
    }
    
    
     //Releases all the objects retained by the IloCplex object.
    
    public void end(){
        model.end();
    }
    
}
