package Assignment2;

/**
 *
 * @author Gustav
 */

import ilog.concert.IloException;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


public class Main {
    
    /**
     * @param args the command line arguments
     * @throws ilog.concert.IloException
     * @throws java.io.FileNotFoundException
     */
    
    public static void main(String[] args) throws IloException, FileNotFoundException{
        //Populates the data of the problem
        int nUnits = 31;
        int nTimePeriods = 24;
        double loadSheddingCosts[] = new double[nTimePeriods];
        for(int t = 1; t <= nTimePeriods; t++){
            loadSheddingCosts[t-1] = 46;
        } 
        //Create a scanner for each of the two files
        //Prepare the elements we will populate when scanning the files
        File generators = new File("generators.txt");
        Scanner scanner1 = new Scanner(generators);
        File loadsfile = new File("loads.txt");
        Scanner scanner2 = new Scanner(loadsfile); 
        double startupCosts[] = new double[nUnits];
        double commitmentCosts[] = new double[nUnits];
        double productionCosts[] = new double[nUnits];
        double loads[] = new double[nTimePeriods];
        double powerLB[] = new double[nUnits];
        double powerUB[] = new double[nUnits];
        double ramping[] = new double[nUnits];
        double minUptime[] = new double[nUnits];
        double minDowntime[] = new double[nUnits];
        
        //Scanning the generator file
        scanner1.nextLine();
        for(int g = 1 ; g <= nUnits; g++){
            scanner1.next();
            powerLB[g-1] = scanner1.nextDouble();
            powerUB[g-1] = scanner1.nextDouble();
            startupCosts[g-1] = scanner1.nextDouble();
            commitmentCosts[g-1] = scanner1.nextDouble();
            ramping[g-1] = scanner1.nextDouble();
            minUptime[g-1] = scanner1.nextDouble();
            minDowntime[g-1] = scanner1.nextDouble();
            productionCosts[g-1] = scanner1.nextDouble();
        }
        //Scanning the loads file
        scanner2.nextLine();
        for(int t = 1; t <= nTimePeriods; t++){
            loads[t-1] = scanner2.nextDouble();
        }
    
        // Creates an instance of the capacity planning problem
        
        UnitCommitmentProblem ucp = new UnitCommitmentProblem(nUnits,nTimePeriods,startupCosts,
                commitmentCosts,productionCosts,loads,powerLB,powerUB,ramping,minUptime,minDowntime,loadSheddingCosts);
        
        // Creates the Master Problem
        MasterProblem mp = new MasterProblem(ucp);
        // Solves the Master Problem and prints solution
        mp.solve();
        System.out.println("Optimal Benders objective value = "+mp.getObjective());
        
       
        // Checks if the results are consistent with solving the full problem
        FullModel model = new FullModel(ucp);
        model.solve();
       
    }
    
}

