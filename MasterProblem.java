package Assignment2;
/**
 *
 * @author Gustav
 */

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloRange;
import ilog.cplex.IloCplex;

public class MasterProblem {
    
    private final IloCplex model;
    private final IloIntVar u[][];
    private final IloNumVar c[][];
    private final IloNumVar phi;
    private final UnitCommitmentProblem ucp;
    private final IloRange startupCostConstraint[][];
    private final IloRange minUptimeConstraint[][];
    private final IloRange minDowntimeConstraint[][];

    /**
     * Creates the Master Problem.
     * @param ucp
     * @throws IloException 
     */
    public MasterProblem(UnitCommitmentProblem ucp) throws IloException {
        this.ucp = ucp;
        
        // 1. Every model needs an IloCplex object
        this.model = new IloCplex();
        
        // 2. Creates the decision variables
        this.u = new IloIntVar[ucp.getnUnits()][ucp.getnTimePeriods()];
        
        for(int g = 1; g<= ucp.getnUnits(); g++){
            for(int t = 1; t<= ucp.getnTimePeriods(); t++){
            u[g-1][t-1] = model.boolVar();
            }
        }
        
        this.c = new IloNumVar[ucp.getnUnits()][ucp.getnTimePeriods()];
        
        for(int g = 1; g<= ucp.getnUnits(); g++){
            for(int t = 1; t<= ucp.getnTimePeriods(); t++){
            c[g-1][t-1] = model.numVar(0,Double.POSITIVE_INFINITY,"c");
            }
        }
             
        this.phi = model.numVar(0, Double.POSITIVE_INFINITY,"phi");
        
        // 3. Creates the objective function
        // Creates an empty linear numerical expression (linear equation)
        IloLinearNumExpr obj = model.linearNumExpr();
        
        // Adds terms to the equation
        for(int g = 1; g<= ucp.getnUnits(); g++){
            for(int t = 1; t<= ucp.getnTimePeriods(); t++){
                obj.addTerm(1,c[g-1][t-1]);
                obj.addTerm(ucp.getCommitmentCosts()[g-1],u[g-1][t-1]);                
            }
        }
        obj.addTerm(1, phi);
        
        // Tells cplex to minimize the objective function
        model.addMinimize(obj);
        
        // 4. Creates the constraints
        // For each constraints creates an populates a linear equation
             
        // Startup cost constraint
        this.startupCostConstraint = new IloRange[ucp.getnUnits()][ucp.getnTimePeriods()];
        for(int g = 1; g<= ucp.getnUnits(); g++){
            for(int t = 1; t<= ucp.getnTimePeriods(); t++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(1, c[g-1][t-1]);
                IloLinearNumExpr rhs = model.linearNumExpr();
                rhs.addTerm(ucp.getStartupCosts()[g-1], u[g-1][t-1]);
                if(t >= 2){rhs.addTerm(-ucp.getStartupCosts()[g-1], u[g-1][t-2]);}else{}
                model.addGe(lhs, rhs); 
                startupCostConstraint[g-1][t-1] = (IloRange) model.addGe(lhs, rhs);
            }
        }
        
        // Minimum operation time constraint    
        this.minUptimeConstraint = new IloRange[ucp.getnUnits()][ucp.getnTimePeriods()];    
        for(int g = 1; g<= ucp.getnUnits(); g++){
            for(int t = 1; t<= ucp.getnTimePeriods(); t++){
                 IloLinearNumExpr lhs = model.linearNumExpr();
                 for(int i = t; i<= ucp.getMinUptime()[g-1]; i++){
                 lhs.addTerm(1, u[g-1][i-1]);
                 lhs.addTerm(-1, u[g-1][t-1]);
                 if(t >= 2){ lhs.addTerm(1, u[g-1][t-2]);}else{}
                 model.addGe(lhs, 0);    
                 } 
                 minUptimeConstraint[g-1][t-1] = (IloRange) model.addGe(lhs,0); 
            }
        }
        
        // Minimum down time constraint
        this.minDowntimeConstraint = new IloRange[ucp.getnUnits()][ucp.getnTimePeriods()];
        for(int g = 1; g<= ucp.getnUnits(); g++){
            for(int t = 1; t<= ucp.getnTimePeriods(); t++){
                 IloLinearNumExpr lhs = model.linearNumExpr();
                 for(int i = t; i<= ucp.getMinUptime()[g-1]; i++){
                 lhs.setConstant(1);
                 lhs.addTerm(1, u[g-1][i-1]);
                 lhs.addTerm(-1, u[g-1][t-1]);
                 if(t >= 2){lhs.addTerm(1, u[g-1][t-2]);}else{}
                 model.addGe(lhs, 0);    
                 } 
                 minDowntimeConstraint[g-1][t-1] = (IloRange) model.addGe(lhs,0);
            }
        }
    }
      
    /**
     * Solves the Master Problem.
     * @throws IloException 
     */
    public void solve() throws IloException{
        
        // we tell Cplex to use the callback that we define below
        model.use(new Callback());
        
        // Solves the problem
        model.solve();
    }
    
    /**
     * The class Callback extends the LazyConstraintCallback. 
     * This makes it so that each time we reach an integer node
     * we use the callback method. Therefore we code all the actions from 
     * Benders in the callback class.
     */
    private class Callback extends IloCplex.LazyConstraintCallback{

        public Callback() {
        }
        /**
         * This is the main method of the Callback class.
         * Whatever we code in this method will be run every time
         * the B&B method reaches an integer node. Here we 
         * code the routines necessary to verify whether we need
         * cuts and, in case, generate and add cuts. 
         * @throws IloException 
         */
        @Override
        protected void main() throws IloException {
            // 1. We start by obtaining the solution at the current node
            double[][] U = getU();
            double Phi = getPhi();
            
            // 2. We check feasibility of the subproblem 
            // 2.1 We create and solve a feasibility subproblem 
            FeasibilitySubProblem fsp = new FeasibilitySubProblem(ucp,U);
            fsp.solve();
            double fspObjective = fsp.getObjective();
            
            // 2.2 We check if the suproblem is feasible. 
            // If the objective is zero the subproblem is feasible
            System.out.println("FSP "+fspObjective);
            if(fspObjective >= 0+1e-9){
                // 2.3 If the objective is positive 
                // the subproblem is not feasible. Thus we 
                // need a feasibility cut.
                System.out.println("Generating feasibility cut");
                // 2.4 We obtain the constant and the linear term of the cut
                // from the feasibility subproblem
                double constant = fsp.getCutConstant();
                IloLinearNumExpr linearTerm = fsp.getCutLinearTerm(u);
                
                // 2.5 Thus we generate and add a cut to the current model.
               
                add(model.le(linearTerm, -constant));
            }else{
                // 3. Since the subproblem is feasible, we check optimality
                // and verify whether we should add an optimality cut.
                
                // 3.1. First, we create and solve an optimality suproblem
                OptimalitySubProblem osp = new OptimalitySubProblem(ucp,U);
                osp.solve();
                double ospObjective = osp.getObjective();
                
                // 3.2. Then we check if the optimality test is satisfied.
                System.out.println("Phi "+Phi+ " OSP "+ospObjective );
                if(Phi >= ospObjective - 1e-9){
                    // 3.3. In this case the problem at the current node
                    // is optimal.
                    System.out.println("The current node is optimal");
                }else{
                    // 3.4. In this case we need an optimality cut. 
                    System.out.println("Generating optimality cut");
                    // We get the constant and the linear term from
                    // the optimality suproblem 
                    double cutConstant = osp.getCutConstant();
                    IloLinearNumExpr cutTerm = osp.getCutLinearTerm(u);
                    cutTerm.addTerm(-1, phi);
                    // and generate and add a cut. 
                    add(model.le(cutTerm, -cutConstant));
                }
            }
        }
        /**
        * Returns the U component of the solution at the current B&B integer node
        * @return the U component of the current solution
        * @throws IloException 
        */
        public double[][] getU() throws IloException{
           double U[][] = new double[ucp.getnUnits()][ucp.getnTimePeriods()];
           for(int g = 1; g<= ucp.getnUnits() ;g++){
               for(int t = 1; t<= ucp.getnTimePeriods(); t++){
                U[g-1][t-1] = getValue(u[g-1][t-1]);
               }
           }
           return U;
       }
       /**
        * Returns the value of phi at the current B&B integer node.
        * @return the value of phi.
        * @throws IloException 
        */
        public double getPhi() throws IloException{
            return getValue(phi);
        }
    }
    
    
    /**
     * Returns the objective value
     * @return
     * @throws IloException 
     */
    public double getObjective() throws IloException{
        return model.getObjValue();
    }
    
    //Releases all the objects retained by the IloCplex object.
    
    public void end(){
        model.end();
    }
    
}

