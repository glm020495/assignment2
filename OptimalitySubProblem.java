package Assignment2;

/**
 *
 * @author Gustav
 */

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloRange;
import ilog.cplex.IloCplex;

public class OptimalitySubProblem {
    
    private final IloCplex model;
    private final IloNumVar p[][];
    private final IloNumVar l[];
    private final UnitCommitmentProblem ucp;
    private final IloRange powerBalanceConstraint[];
    private final IloRange minProductionConstraint[][];
    private final IloRange maxProductionConstraint[][];
    private final IloRange rampUpConstraint[][];
    private final IloRange rampDownConstraint[][];
    
    /**
     * Creates the model for the optimality subproblem
     * @param ucp
     * @param U a part of the solution to MP
     * @throws IloException 
     */
    public OptimalitySubProblem(UnitCommitmentProblem ucp, double U[][]) throws IloException {
        this.ucp = ucp;
        
        
        // 1. Every model needs an IloCplex object
        this.model = new IloCplex();
        
        // 2. Creates the decision variables
        this.p = new IloNumVar[ucp.getnUnits()][ucp.getnTimePeriods()];
        
        for(int g = 1; g<= ucp.getnUnits(); g++){
            for(int t = 1; t<= ucp.getnTimePeriods(); t++){
            p[g-1][t-1] = model.numVar(0,Double.POSITIVE_INFINITY,"p");
            }
        }
        
         this.l = new IloNumVar[ucp.getnTimePeriods()];
        
        for(int t = 1; t<= ucp.getnTimePeriods(); t++){
            l[t-1] = model.numVar(0,Double.POSITIVE_INFINITY,"l");
        }
        
       // 3. Creates the objective function
        // Creates an empty linear numerical expression (linear equation)
        IloLinearNumExpr obj = model.linearNumExpr();
        
        // Adds terms to the equation
        for(int g = 1; g<= ucp.getnUnits(); g++){
            for(int t = 1; t<= ucp.getnTimePeriods(); t++){
                obj.addTerm(ucp.getProductionCosts()[g-1],p[g-1][t-1]); 
            }
        }
        for(int t = 1; t<= ucp.getnTimePeriods(); t++){
            obj.addTerm(ucp.getLoadSheddingCost()[t-1],l[t-1]);            
        }
                
        // Tells cplex to minimize the objective function
        model.addMinimize(obj);
      
        // 4. Creates the constraints
        // For each constraints creates an populates a linear equation
        
        // Power balance constraint
        this.powerBalanceConstraint = new IloRange[ucp.getnTimePeriods()];
        for(int t = 1; t<= ucp.getnTimePeriods(); t++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                for(int g = 1; g<= ucp.getnUnits(); g++){
                    lhs.addTerm(1, p[g-1][t-1]);
                }
                lhs.addTerm(1, l[t-1]);
                powerBalanceConstraint[t-1] = (IloRange) model.addEq(lhs, ucp.getLoads()[t-1]);
            }
        
        
        // Minimum production constraint
        this.minProductionConstraint = new IloRange[ucp.getnUnits()][ucp.getnTimePeriods()];
        for(int g = 1; g<= ucp.getnUnits(); g++){
            for(int t = 1; t<= ucp.getnTimePeriods(); t++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(1, p[g-1][t-1]);
                minProductionConstraint[g-1][t-1] = (IloRange) model.addGe(lhs, ucp.getPowerLB()[g-1]*U[g-1][t-1]);
            }
        }
        
        // Maximum production constraint
        this.maxProductionConstraint = new IloRange[ucp.getnUnits()][ucp.getnTimePeriods()];
        for(int g = 1; g<= ucp.getnUnits(); g++){
            for(int t = 1; t<= ucp.getnTimePeriods(); t++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(1, p[g-1][t-1]);
                maxProductionConstraint[g-1][t-1] = (IloRange) model.addLe(lhs, ucp.getPowerUB()[g-1]*U[g-1][t-1]);
            }
        }
        
        // Ramp-up limit constraint
        this.rampUpConstraint = new IloRange[ucp.getnUnits()][ucp.getnTimePeriods()];
        for(int g = 1; g<= ucp.getnUnits(); g++){
            for(int t = 1; t<= ucp.getnTimePeriods(); t++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(1, p[g-1][t-1]);
                if(t >= 2){lhs.addTerm(-1, p[g-1][t-2]);}else{}
                rampUpConstraint[g-1][t-1] = (IloRange) model.addLe(lhs, ucp.getRamping()[g-1]);
            }
        }
        
        // Ramp-down limit constraint
        this.rampDownConstraint = new IloRange[ucp.getnUnits()][ucp.getnTimePeriods()];
        for(int g = 1; g<= ucp.getnUnits(); g++){
            for(int t = 1; t<= ucp.getnTimePeriods(); t++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                if(t >= 2){lhs.addTerm(1, p[g-1][t-2]);}else{}
                lhs.addTerm(-1, p[g-1][t-1]);
                rampDownConstraint[g-1][t-1] = (IloRange) model.addLe(lhs, ucp.getRamping()[g-1]);
            }
        }
    }
    
    // solves the problem
    public void solve() throws IloException{
        model.setOut(null);
        model.solve();
    }
    
    /**
     * Returns the objective value
     * @return the objective value
     * @throws IloException 
     */
    public double getObjective() throws IloException{
        return model.getObjValue();
    }
    
    /**
     * Returns the constant part of the optimality cut.
     * That is, the part of the cut not dependent on u.
     * @return the constant of the cut
     * @throws IloException 
     */
    public double getCutConstant() throws IloException{
        double constant = 0;
        for(int t = 1; t <= ucp.getnTimePeriods(); t++){
            constant = constant + model.getDual(powerBalanceConstraint[t-1]) * ucp.getLoads()[t-1];
            for(int g = 1; g <= ucp.getnUnits(); g++){
                constant = constant + model.getDual(rampUpConstraint[g-1][t-1]) * ucp.getRamping()[g-1] + model.getDual(rampDownConstraint[g-1][t-1]) * ucp.getRamping()[g-1];
            }
        }    
        return constant;
    }
    /**
     * Returns the linear expression in u of the optimality cut.
     * @param u the u variables of the master problem
     * @return the linear term of the cut
     * @throws IloException 
     */
    
    public IloLinearNumExpr getCutLinearTerm(IloIntVar u[][]) throws IloException{
        IloLinearNumExpr cutTerm = model.linearNumExpr();
        
        for(int g = 1; g <= ucp.getnUnits(); g++){
            for(int t = 1; t <= ucp.getnTimePeriods(); t++){
            cutTerm.addTerm(model.getDual(minProductionConstraint[g-1][t-1]) * ucp.getPowerLB()[g-1], u[g-1][t-1]); 
            cutTerm.addTerm(model.getDual(maxProductionConstraint[g-1][t-1]) * ucp.getPowerUB()[g-1], u[g-1][t-1]);
            }
        }    
        return cutTerm;
    }
    
    //Releases all the objects retained by the IloCplex object.
     
    
    public void end(){
        model.end();
    }
    
}
