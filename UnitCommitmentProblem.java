package Assignment2;

/**
 *
 * @author Gustav
 */
public class UnitCommitmentProblem {
    // Lists all the elements needed in an Unit Commitment Problem
    private final int nUnits;
    private final int nTimePeriods;
    private final double startupCosts[];
    private final double commitmentCosts[];
    private final double productionCosts[];
    private final double loads[];
    private final double powerLB[];
    private final double powerUB[];
    private final double ramping[];
    private final double minUptime[];
    private final double minDowntime[];
    private final double loadSheddingCost[];

    //Creates the constructor for all the elements
    public UnitCommitmentProblem(int nUnits, int nTimePeriods, double[] startupCosts, double[] commitmentCosts, double[] productionCosts, double[] loads, double[] powerLB, double[] powerUB, double[] ramping, double[] minUptime, double[] minDowntime, double[] loadSheddingCost) {
        this.nUnits = nUnits;
        this.nTimePeriods = nTimePeriods;
        this.startupCosts = startupCosts;
        this.commitmentCosts = commitmentCosts;
        this.productionCosts = productionCosts;
        this.loads = loads;
        this.powerLB = powerLB;
        this.powerUB = powerUB;
        this.ramping = ramping;
        this.minUptime = minUptime;
        this.minDowntime = minDowntime;
        this.loadSheddingCost = loadSheddingCost;
    }

    UnitCommitmentProblem(int nUnits, int nTimePeriods, double[] startupCosts, double[] commitmentCosts, double[] productionCosts, double[] loads, double[] powerLB, double[] powerUB, double[] ramping, double[] minUptime, double[] minDowntime) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    // Creates a getter for each element
    public int getnUnits() {
        return nUnits;
    }

    public int getnTimePeriods() {
        return nTimePeriods;
    }

    public double[] getStartupCosts() {
        return startupCosts;
    }

    public double[] getCommitmentCosts() {
        return commitmentCosts;
    }

    public double[] getProductionCosts() {
        return productionCosts;
    }

    public double[] getLoads() {
        return loads;
    }

    public double[] getPowerLB() {
        return powerLB;
    }

    public double[] getPowerUB() {
        return powerUB;
    }

    public double[] getRamping() {
        return ramping;
    }

    public double[] getMinUptime() {
        return minUptime;
    }

    public double[] getMinDowntime() {
        return minDowntime;
    }

    public double[] getLoadSheddingCost() {
        return loadSheddingCost;
    }

   
    
    
}


